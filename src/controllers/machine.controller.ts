import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,
  patch, post,
  put,
  requestBody
} from '@loopback/rest';
import fetch from 'node-fetch';
import {Machine} from '../models';
import {MachineRepository} from '../repositories';
const ACCESS_TOKEN = '711990409453194|6jmPtWiNi5GKLIUyBU998Fuur2A';
let errorMessage = {
  message: 'Authenticate error',
  messageCode: '401'
};

export class MachineController {
  constructor(
    @repository(MachineRepository)
    public machineRepository: MachineRepository,
  ) {
  }

  @post('/machines', {
    responses: {
      '200': {
        description: 'Machine model instance',
        content: {'application/json': {schema: getModelSchemaRef(Machine)}},
      },
    },
  })

  async FBToken(): Promise<String> {
    var apiURL = 'https://graph.facebook.com/oauth/access_token?client_id=711990409453194&client_secret=8601b0ddb811f1850f69f7ee635551ea&grant_type=client_credentials'
    return fetch(apiURL, {
      method: 'get',
      headers: {'Content-Type': 'application/json'},
    })
      .then(response => response.json())
      .then((body) => {return body.access_token})
      .catch((error: Error) => {console.log(error)});
  }

  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Machine, {
            title: 'NewMachine',
            exclude: ['id'],
          }),
        },
      },
    })
    machine: Omit<Machine, 'id'>,
  ): Promise<Machine> {
    return this.machineRepository.create(machine);
  }

  @get('/machines/count', {
    responses: {
      '200': {
        description: 'Machine model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })

  async count(
    @param.where(Machine) where?: Where<Machine>,
  ): Promise<Count> {
    console.log('*** get fb token ***');
    const accessToken: string = ACCESS_TOKEN;
    if (accessToken !== await this.FBToken()) {
      console.log('not authen');
    }
    return this.machineRepository.count(where);
  }

  @get('/machines', {
    responses: {
      '200': {
        description: 'Array of Machine model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Machine, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Machine) filter?: Filter<Machine>,
  ): Promise<Machine[]> {
    return this.machineRepository.find(filter);
  }

  @patch('/machines', {
    responses: {
      '200': {
        description: 'Machine PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Machine, {partial: true}),
        },
      },
    })
    machine: Machine,
    @param.where(Machine) where?: Where<Machine>,
  ): Promise<Count> {
    return this.machineRepository.updateAll(machine, where);
  }

  @get('/machines/{id}', {
    responses: {
      '200': {
        description: 'Machine model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Machine, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Machine, {exclude: 'where'}) filter?: FilterExcludingWhere<Machine>
  ): Promise<Machine> {
    return this.machineRepository.findById(id, filter);
  }

  @patch('/machines/{id}', {
    responses: {
      '204': {
        description: 'Machine PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Machine, {partial: true}),
        },
      },
    })
    machine: Machine,
  ): Promise<void> {
    await this.machineRepository.updateById(id, machine);
  }

  @put('/machines/{id}', {
    responses: {
      '204': {
        description: 'Machine PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() machine: Machine,
  ): Promise<void> {
    await this.machineRepository.replaceById(id, machine);
  }

  @del('/machines/{id}', {
    responses: {
      '204': {
        description: 'Machine DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.machineRepository.deleteById(id);
  }
}
