import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Machine extends Entity {

  @property({
    type: 'string',
    id: true,
    generated: true
  })
  machineID?: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'Object',
  })
  attributes?: Object;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Machine>) {
    super(data);
  }
}

export interface MachineRelations {
  // describe navigational properties here
}

export type MachineWithRelations = Machine & MachineRelations;
