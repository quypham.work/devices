import {Model, model, property} from '@loopback/repository';

@model()
export class FbUser extends Model {

  constructor(data?: Partial<FbUser>) {
    super(data);
  }
}

export interface FbUserRelations {
  // describe navigational properties here
}

export type FbUserWithRelations = FbUser & FbUserRelations;
