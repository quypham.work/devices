import {DefaultCrudRepository} from '@loopback/repository';
import {Machine, MachineRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MachineRepository extends DefaultCrudRepository<
  Machine,
  typeof Machine.prototype.machineID,
  MachineRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Machine, dataSource);
  }
}
